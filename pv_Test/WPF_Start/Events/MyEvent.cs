﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WPF_Start.Events
{

    public enum EVENT_TYPE { EVENT_REGISTER, EVENT_DIAGNOSE, EVENT_UNDEFINED }


    class MyEvent
    {
        private string _eventType;

        public string eventType
        {
            set { _eventType = value; }
            get { return _eventType; }
        }

        public string eventDescription;

        public int[] dividerArray;
        public string[] stringArray;
        public substituteDetail[] sortedArray;


        public MyEvent(EVENT_TYPE et)
        {
            string strPatient = "Patient";
            substituteDetail[] subArray;

            switch (et)
            {
                case EVENT_TYPE.EVENT_REGISTER:

                    eventType = "&quot;Register&quote;";
                    eventDescription = "Register";
                    subArray = new substituteDetail[3] { new substituteDetail(15, string.Format("{0} {1}", eventDescription, strPatient)), new substituteDetail(5, strPatient), new substituteDetail(3, eventDescription) };
                    break;

                case EVENT_TYPE.EVENT_DIAGNOSE:

                    eventType = "&quote;Diagnose&quote;";
                    eventDescription = "Diagnose";
                    subArray = new substituteDetail[3] { new substituteDetail(14, string.Format("{0} {1}", eventDescription, strPatient)), new substituteDetail(7, strPatient), new substituteDetail(2, eventDescription) };
                    break;

                default:
                    eventType = "undefined";
                    eventDescription = "Undefined";
                    subArray = new substituteDetail[1]{new substituteDetail(0, "Something wrong with data")};
                    break;
            }
                   
                   sortedArray = subArray.OrderByDescending(c => c.numb).ToArray();
            
        }
    }
}
