﻿using System;
using System.Collections.Generic;
using System.Text;
using WPF_Start.Commands;
using WPF_Start.Models;
using WPF_Start.Events;
using System.Diagnostics;
using System.Windows.Input;
using System.Linq;


namespace WPF_Start.ViewModels
{
    class processEventViewModel
    {
      //  public delegate void myAction();
        public processEventViewModel()
        {
            _mm1 = new processEventModel("n/a.", "");

            Action<object> myAct = new Action<object>(ProcessEvent);
            Action<object> myClear = new Action<object>(clearEvent);
            
            eventGenerateCommand = new RelayCommand(myAct);        
            clearCommand = new RelayCommand(myClear);
                    
        }

        private processEventModel _mm1;
        private MyEvent theEvent;


        public processEventModel mm1
        {

            get { return _mm1; }
            set { _mm1 = value; }

        }

        public bool CanUpdate
        {
            get
            {
                if (mm1 == null)
                    return false;

                return !string.IsNullOrWhiteSpace(mm1.evType);
            }
        }
        

        public ICommand eventGenerateCommand
        {
            private set;
            get;
        }


        public ICommand clearCommand
        {
            private set;
            get;
        }

        ///<summary>
        ///summary description
        /// function  GenerateEvent generate randomly either register or Diagnose event.
        /// Since it is a test and I need to have some events to process I desided that it is one of the posibilities to get event.
        ///<remarks>
        ///
        ///</remarks>
        private void GenerateEvent()
        {
            Random rnd = new Random();
            var myEnumMemberCount = Enum.GetNames(typeof(EVENT_TYPE)).Length;
            int numb = rnd.Next(myEnumMemberCount - 1);

            theEvent = new MyEvent((EVENT_TYPE)numb);
        }

        ///<summary>
        ///summary description
        /// function  ProcessEvent   calls GenerateEvent to generate event and then process it.
        /// since two consecutive events might be the same type, I add the count on the screen so one can see that the next even was also processed.
        /// 
        ///<remarks>
        ///
        ///</remarks>
        public void ProcessEvent(object obj)
        {
            GenerateEvent();

            if (theEvent.eventDescription.Equals("Undefined") || theEvent.sortedArray == null  || theEvent.sortedArray.Count() != 3)
                return;

            int[] rawResult = Enumerable.Range(0, 100).Select(i => i + 1).ToArray();
            string ii;

            //theEvent.sortedArray  keeps pairs of (divider, string to put instead of number)
            //it is possible that foreach el in theEvent.sortedArray  this pice of code might look better.
            string[] newarry = rawResult
         .Select(i =>
              {
             if (i % theEvent.sortedArray[0].numb == 0)
                 ii = theEvent.sortedArray[0].descr;
             else if (i % theEvent.sortedArray[1].numb == 0)
                ii = theEvent.sortedArray[1].descr;
             else if (i % theEvent.sortedArray[2].numb == 0)
                 ii = theEvent.sortedArray[2].descr;
             else
                 ii = i.ToString();
             return ii;
         })
           .ToArray();

            mm1.evType = theEvent.eventDescription;
            mm1.evResult = string.Join(" ", newarry);
            int j = 0;
            if (Int32.TryParse(mm1.countStr, out j))
                j++;

            mm1.countStr = j.ToString();

        }

        ///<summary>
        ///summary description
        /// function  clearEvent   clear Event Type, event count and result string from screen.
        /// 
        ///<remarks>
        ///
        ///</remarks>
        public void clearEvent(object obj)
        {
            mm1.evType = "n/a";
            mm1.evResult = "";
            mm1.countStr = "0";
        }
    }

}
