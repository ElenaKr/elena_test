﻿using System;
using System.Text;
using System.ComponentModel;

namespace WPF_Start.Models
{
    public class processEventModel : INotifyPropertyChanged
    {
        private string _evType;
        private string _evResult;
        private string _countStr;

        public processEventModel(string str1, string str2)
        {
            _evType = str1;
            _evResult = str2;
            _countStr = "0";
        }

        public string evType
        {
            get { return _evType; }

            set
            {
                if (_evType != null)
                {

                    _evType = value;
                    PropertyChanged(this, new PropertyChangedEventArgs("evType"));

                }
            }
        }

        public string evResult
        {
            get { return _evResult; }

            set
            {
                _evResult = value;
                PropertyChanged(this, new PropertyChangedEventArgs("evResult"));
            }
        }

        public string countStr
        {
            get { return _countStr; }

            set
            {
                if (_countStr != null)
                {

                    _countStr = value;
                    PropertyChanged(this, new PropertyChangedEventArgs("countStr"));

                }
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string vName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(vName));
            }
        }

    }

}
